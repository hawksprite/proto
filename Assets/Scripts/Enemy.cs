﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public static float speed = 15.0f;
    private Vector3 v;

	// Use this for initialization
	void Start () {
        speed = 3.0f;
        speed += Random.Range(-1, 1);

        if (speed <= 0)
        {
            speed = 1.0f;
        }
	}
	
	// Update is called once per frame
	void Update () {
        // Look at the player
        Vector3 targetDir = Globals.playerLocation - transform.position;
        float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        v = new Vector3((float)Mathf.Cos(angle), (float)Mathf.Sin(angle), 0);

        transform.position = Vector3.MoveTowards(transform.position, Globals.playerLocation, speed * Time.smoothDeltaTime); // transform.position + (v * speed * Time.smoothDeltaTime);

        if (Vector3.Distance(transform.position, Globals.playerLocation) <= 1.0f)
        {
            Application.LoadLevel(3);
            Globals.playerGold = 0;
        }
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "ammo")
        {
            Globals.playerScore++;
            Globals.playerGold += 5;
            Globals.enemyAlive--;
            GameObject.Destroy(this.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            Application.LoadLevel(3);
        }
    }
}