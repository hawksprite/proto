﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SpawnManager : MonoBehaviour {

    public Vector2 maxRegion;

    public GameObject enemyPrefab;

    public int delay = 300;

    public int seed = 10;

    public bool finished = false;

    DateTime lastSpawn;

    List<GameObject> linkL = new List<GameObject>();

	// Use this for initialization
	void Start () {
        lastSpawn = DateTime.Now;

        seed = Globals.currentSeed;
	}
	
	// Update is called once per frame
	void Update () {
        if (!finished)
        {
            if ((DateTime.Now - lastSpawn).TotalMilliseconds >= delay)
            {
                if (seed > 0)
                {
                    lastSpawn = DateTime.Now;

                    Vector3 spawn = new Vector3(
                        UnityEngine.Random.Range(0, maxRegion.x),
                        UnityEngine.Random.Range(0, maxRegion.y),
                        0);

                    GameObject g = GameObject.Instantiate(enemyPrefab, spawn, Quaternion.identity) as GameObject;
                    linkL.Add(g);
                    Globals.enemyAlive++;

                    seed -= 1;
                }
                else
                {
                    finished = true;
                }
            }
        }
        else
        {
            if (linkL.Count <= 0)
            {
                Globals.playerScore = 0;
                // Go to upgrade scene
                Application.LoadLevel(2);
            }
        }


        // FLesh out all the bad pointers from our link list
        List<GameObject> nL = new List<GameObject>();

        foreach (GameObject g in linkL)
        {
            if (g)
            {
                nL.Add(g);
            }
        }

        linkL = nL;
	}
}
