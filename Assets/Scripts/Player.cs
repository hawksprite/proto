﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using System;

public class Player : MonoBehaviour {

    public GameObject ammoPrefab;

    public float speed = 20.0f;

    private Vector3 v;

    public float shootDelay = 100;

	// Use this for initialization
	void Start () {
        lastShot = DateTime.Now;
        lastShot2 = DateTime.Now;
        Input.gyro.enabled = true;

        shootDelay = 200 * Globals.playerGunMultiplier;

        speed = speed + Globals.movementSpeedBuff;
	}
	
	// Update is called once per frame
	void Update () {
        Globals.playerLocation = transform.position;

        if (Input.GetMouseButton(0))
        {
            Vector3 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            target.z = transform.position.z;

            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.smoothDeltaTime);

            Vector3 targetDir = target - transform.position;
            float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0, 0, angle);

            Shoot(angle, Globals.playerShotSpread);
                
        }

        for (int i = 0; i < Input.touches.Length; i++)
        {
            if (Input.touches[i].tapCount >= 2)
            {
                Orange();
            }
        }

        

        // Transfer distance
        float distance = Vector3.Distance(Vector3.zero, transform.position);

        float p = distance / 100.0f;

        p *= 1.6f;

        Camera.main.backgroundColor = Color.Lerp(Color.white, Color.black, p);
	}

    DateTime lastShot;
    public void Shoot(float t, int c)
    {
        if ((DateTime.Now - lastShot).TotalMilliseconds >= shootDelay)
        {
            for (int i = 0; i < c; i++)
            {
                lastShot = DateTime.Now;
                GameObject g = GameObject.Instantiate(ammoPrefab, transform.position, Quaternion.identity) as GameObject;
                g.GetComponent<Projectile>().Fire(t + UnityEngine.Random.Range(-1.5f * c, 1.5f * c));
            }
        }
    }

    DateTime lastShot2;
    public void Orange()
    {
        if ((DateTime.Now - lastShot2).TotalMilliseconds >= 400)
        {
            lastShot2 = DateTime.Now;

            int size = 20;

            float par = size / 360;

            for (int i = 0; i < size; i++)
            {
                float a = par * i;

                GameObject g = GameObject.Instantiate(ammoPrefab, transform.position, Quaternion.identity) as GameObject;
                g.GetComponent<Projectile>().Fire(a);
            }
        }
    }
}
