﻿using UnityEngine;
using System.Collections;
using System;

public class Projectile : MonoBehaviour {

    public float speed = 20.0f;

	// Use this for initialization
	void Start () {
        spawnT = DateTime.Now;
	}

    private DateTime spawnT;
    private float t;
    private Vector3 v;
    private bool r = false;
	// Update is called once per frame
	void Update () {
        if (r)
        {
            // Move forward
            transform.position = (transform.position + (v * speed * Time.smoothDeltaTime));
        }

        // Die after 2 seconds
        if ((DateTime.Now - spawnT).TotalSeconds >= 2)
        {
            Destroy(this.gameObject);
        }
	}

    public void Fire(float target)
    {
        t = target;
        r = true;

        t *= Mathf.Deg2Rad;

        v = new Vector3((float)Mathf.Cos(t), (float)Mathf.Sin(t), 0);

        transform.rotation = Quaternion.Euler(0, 0, t);
    }
}
