﻿using UnityEngine;
using System.Collections;

public static class Globals  {
    public static Vector3 playerLocation;

    public static int playerGold = 0;
    public static int playerScore = 0;

    public static int enemyAlive = 0;

    public static int currentSeed = 10;

    public static float playerGunMultiplier = 1.0f;

    public static int playerShotSpread = 1;

    public static float movementSpeedBuff = 0;
}
