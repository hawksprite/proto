﻿using UnityEngine;
using System.Collections;

public class HomeButton : MonoBehaviour {

    public bool useTouch = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (useTouch)
        {
            if (Input.touchCount > 0)
            {
                Home();
            }
        }
	}

    public void Home()
    {
        Application.LoadLevel(0);
    }
}
