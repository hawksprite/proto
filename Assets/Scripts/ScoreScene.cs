﻿using UnityEngine;
using System.Collections;

public class ScoreScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
        // Double scene for next level
        Globals.currentSeed *= 2;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void NextLevel()
    {
        Globals.playerScore = 0;
        Application.LoadLevel(1);
    }

    public void UpgradeGun()
    {
        if (Globals.playerGold >= 100)
        {
            Globals.playerGold -= 100;
            Globals.playerGunMultiplier -= 0.05f;

            Globals.playerShotSpread++;
        }
    }

    public void UpgradeSpeed()
    {
        if (Globals.playerGold >= 100)
        {
            Globals.playerGold -= 100;

            Globals.movementSpeedBuff += 3;
        }
    }
}
